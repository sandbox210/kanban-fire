// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDdtI-ApiYneFfecWBZn1gN2Dw_bjkj1QE",
    authDomain: "kanban-fire-7a983.firebaseapp.com",
    projectId: "kanban-fire-7a983",
    storageBucket: "kanban-fire-7a983.appspot.com",
    messagingSenderId: "477159159053",
    appId: "1:477159159053:web:1dc985985fa5322806ac44",
    measurementId: "G-TKH2YDKNPH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
